
plugin.tx_phipfelswatchword {
	view {
		templateRootPath = {$plugin.tx_phipfelswatchword.view.templateRootPath}
		partialRootPath = {$plugin.tx_phipfelswatchword.view.partialRootPath}
		layoutRootPath = {$plugin.tx_phipfelswatchword.view.layoutRootPath}
	}
	persistence {
		storagePid = {$plugin.tx_phipfelswatchword.persistence.storagePid}
	}
	features {
		autoDownload = {$plugin.tx_phipfelswatchword.features.autoDownload}
		enablePublicHoliays = {$plugin.tx_phipfelswatchword.features.enablePublicHoliays}
        dateFormatFluid = {$plugin.tx_phipfelswatchword.features.dateFormatFluid}
        downloadUrl = {$plugin.tx_phipfelswatchword.features.downloadUrl}
	}
}

page {
    includeCSS {
        phipfels_watchword_css = EXT:phipfels_watchword/Resources/Public/Css/Frontend/phipfels_watchword.css
        phipfels_watchword_css.if.isTrue = {$plugin.tx_phipfelswatchword.features.includeDemoCss}
    }
}
