<?php

use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

defined('TYPO3') || die('Access denied.');

(static function (): void {
    ExtensionUtility::registerPlugin(
        'phipfels_watchword',
        'Displaywatchword',
        'Herrnhuter Losung'
    );
})();

?>
