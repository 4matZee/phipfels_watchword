<?php

defined('TYPO3') || die('Access denied.');

$GLOBALS['TCA']['tx_phipfelswatchword_domain_model_watchword'] = array(
	'ctrl' => array(
		'title'	=> 'LLL:EXT:phipfels_watchword/Resources/Private/Language/locallang_db.xlf:tx_phipfelswatchword_domain_model_watchword',
		'label' => 'date',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'date',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'date,public_holiday,watchword,watchword_passage,instructive_text,instructive_text_passage',
        'iconfile' => 'EXT:phipfels_watchword/Resources/Public/Icons/watchword.svg',
        'security' => [
            'ignorePageTypeRestriction' => true,
        ],
	),
	'interface' => array(
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, date, public_holiday, watchword, watchword_passage, instructive_text, instructive_text_passage',
	),
	'types' => array(
		'1' => array('showitem' => 'sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, date, public_holiday, watchword, watchword_passage, instructive_text, instructive_text_passage, --div--;LLL:EXT:cms/locallang_ttc.xlf:tabs.access, starttime, endtime'),
	),
	'columns' => array(
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],

		'l10n_parent' => [
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude' => true,
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config' => [
				'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
				'foreign_table' => 'tx_phipfelswatchword_domain_model_watchword',
				'foreign_table_where' => 'AND tx_phipfelswatchword_domain_model_watchword.pid=###CURRENT_PID### AND tx_phipfelswatchword_domain_model_watchword.sys_language_uid IN (-1,0)',
			],
		],

		'l10n_diffsource' => [
			'config' => [
				'type' => 'passthrough',
			],
		],

		't3ver_label' => [
			'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'max' => 255,
			]
		],

        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/Resources/Private/Language/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],

        'starttime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],

        'endtime' => [
            'exclude' => true,
            'behaviour' => [
                'allowLanguageSynchronization' => true
            ],
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

		'date' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:phipfels_watchword/Resources/Private/Language/locallang_db.xlf:tx_phipfelswatchword_domain_model_watchword.date',
			'config' => array(
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'dbType' => 'date',
				'eval' => 'date',
				'default' => '0000-00-00'
			),
		),

		'public_holiday' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:phipfels_watchword/Resources/Private/Language/locallang_db.xlf:tx_phipfelswatchword_domain_model_watchword.public_holiday',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),

		'watchword' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:phipfels_watchword/Resources/Private/Language/locallang_db.xlf:tx_phipfelswatchword_domain_model_watchword.watchword',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),

		'watchword_passage' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:phipfels_watchword/Resources/Private/Language/locallang_db.xlf:tx_phipfelswatchword_domain_model_watchword.watchword_passage',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),

		'instructive_text' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:phipfels_watchword/Resources/Private/Language/locallang_db.xlf:tx_phipfelswatchword_domain_model_watchword.instructive_text',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),

		'instructive_text_passage' => array(
			'exclude' => 1,
			'label' => 'LLL:EXT:phipfels_watchword/Resources/Private/Language/locallang_db.xlf:tx_phipfelswatchword_domain_model_watchword.instructive_text_passage',
			'config' => array(
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			),
		),

	),
);
